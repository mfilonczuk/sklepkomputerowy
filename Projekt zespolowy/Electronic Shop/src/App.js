import "./App.css";
import Header from "./Header.js";
import Payment from "./Payment.js";
import Home from "./Home.js"
import Checkout from "./Checkout";
import Orders from "./Orders"
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Login from "./Login.js"
import { useEffect } from "react";
import { auth } from "./firebase";
import { useStateValue } from "./StateProvider";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";




export default function App() {
  const [{}, dispatch] = useStateValue();

useEffect(() => {
  
  auth.onAuthStateChanged(authUser => {
    console.log('THE USER IS >>>> ', authUser);

    if (authUser){

      dispatch({
        type: "SET_USER",
        user: authUser,
      });

    } else{

      dispatch({
        type: "SET_USER",
        user: null,
      });
    }
  })
}, [])

  return (
    <Router>
        <div className="app">
          <Header/>
          <Elements stripe={loadStripe("pk_test_51L8BfBECZA4zYKMmpFNhkOiUi9vbVwikFLMVAVXdL4xG7VLgpDgvBlOyvvYpDlIBf12f68sUfL8wZPlSROPu4f30008RKNdZNN")}>
            <Routes>
              <Route path = "/" element = {<Home/>}/>
              <Route path = "/checkout" element = {<Checkout/>}/>
              <Route path = "/login" element = {<Login/>}/>
              <Route path = "/payment" element = {<Payment/>} />
              <Route path = "/orders" element = {<Orders/>} />
            </Routes>
          </Elements>
        </div>
    </Router>

  )
  

}
