import React, { useEffect } from "react";
import "./Header.css";
import SearchIcon from '@mui/icons-material/Search';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';
import { Link } from "react-router-dom";
import { useStateValue } from "./StateProvider";
import { auth } from "./firebase";

function Header() {
  const [{ basket, user }, dispatch] = useStateValue();

  const handleAuthentication = () => {
    if (user) {
      auth.signOut();
    }
  };


  return (
    <div className="header">
      <Link to="/">
        <img
          className="header__logo"
          src="https://pngimg.com/uploads/coconut/coconut_PNG9151.png"
          alt="Smile"
        />
      </Link>

      <div className="header__search">
        <input className="header__searchInput" type="text" />
        <SearchIcon className="header__searchIcon" />
        {/* Logo */}
      </div>

      <div className="header__nav">
        {/* if there was no user then you go to login page */}
        {/* Link to was erroring because link to is looking specifically for a string {!user & '/login'} does not return a string if there is a user. */}
        <Link to={!user ? "/login" : "/"}>
          <div onClick={handleAuthentication} className="header__option">
            <span className="header__optionLineOne">
              Witaj! {!user ? "" : user?.email}
            </span>
            <span className="header__optionLineTwo">
              {user ? "Wyloguj się" : "Zaloguj się"}
            </span>
          </div>
        </Link>

        <Link to='/orders'>
        <div className="header__option">
          <span className="header__optionLineOne">Zwroty</span>
          <span className="header__optionLineTwo">& Zamowienia</span>
        </div>
        </Link>

        <div className="header__option">
          <span className="header__optionLineOne">Twoj</span>
          <span className="header__optionLineTwo">Koszyk</span>
        </div>

        <Link to="/checkout">
          <div className="header__optionBasket">
            <ShoppingBasketIcon />
            {/* .? = optional chaining meaning if you for any reason dont have a correct value or undefied it wont break but quietly handle error*/}
            <span className="header__optionLineTwo header__basketCount">
              {basket?.length}
            </span>
          </div>
        </Link>
      </div>
    </div>
  );
}

export default Header;