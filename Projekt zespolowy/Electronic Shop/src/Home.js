import React from 'react';
import Background from "./images/pc2.jpg";
import "./Home.css"
import Product from './Product';

export default function Home() {
  return (
    <div className='home'>
        <div className='home-container'>
            <img src = {Background} className="home-image" />
            <div className='home-row'>
              <Product
                id = "1"
                title = "Odkurzacz Henryczek"
                price = {699}
                image = 'https://euroczystosc.pl/media/catalog/product/cache/1/image/915x915/9df78eab33525d08d6e5fb8d27136e95/5/4/5446076.jpeg'
                rating = {4}
              />
              <Product
                id = "2" 
                title = "Mysz Logitech"
                price = {209}
                image = "https://www.mediaexpert.pl/media/cache/resolve/gallery/images/18/1883439/Mysz-LOGITECH-G502-Lightspeed-front-1x.jpg"
                rating = {5}

              />

            </div>
            <div className='home-row'>
            <Product 
                id = "3" 
                title = "Klawiatura Razer Ornata Chroma v2"
                price = {280}
                image = 'https://prod-api.mediaexpert.pl/api/images/gallery/thumbnails/images/24/2469186/Klawiatura-RAZER-Ornata-Chroma-V2-przod-bez-podporki.jpg'
                rating = {4}
              />
              <Product 
                id = "4" 
                title = "Logitech g pro x"
                price = {280}
                image = 'https://resource.logitechg.com/d_transparent.gif/content/dam/gaming/en/products/pro-x/pro-headset-gallery-1.png'
                rating = {4}
              />
              <Product 
                id = "5"
                title = "iiyama G-Master Black Hawk"
                price = {800}
                image = 'https://cdn.iiyama.com/i/1070x590x1/0a7b8fcbc0a3944438f54df2ece380e4_ge2288hs.png'
                rating = {4}
              />
            </div>
            <div className='home-row'>
            <Product
                id = "6"
                title = "Iiyama G-Master"
                price = {900}
                image = 'https://iiyama-sklep.pl/123060-large_default/monitor-iiyama-g-master-red-eagle-gb3271qsu-wqhd-ips-165hz-1ms-freesync-premium-2xhdmi-2xdp.jpg'
                rating = {5}
              />
            </div>

        </div>

        
    </div>
  )
}
