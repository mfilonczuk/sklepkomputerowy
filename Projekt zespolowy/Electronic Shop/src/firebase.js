// For Firebase JS SDK v7.20.0 and later, measurementId is optional

import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";


const firebaseConfig = {
    apiKey: "AIzaSyA4FfOXxvV2YBkg6Is1MbUMJUVyq85z4Gw",
    authDomain: "electronic-shop-81ebf.firebaseapp.com",
    projectId: "electronic-shop-81ebf",
    storageBucket: "electronic-shop-81ebf.appspot.com",
    messagingSenderId: "78196150849",
    appId: "1:78196150849:web:4fd0c503c216f3b5f52cc0",
    measurementId: "G-36461CL7ZY"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth = firebaseApp.auth();

  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);

  export { db, auth };