# README #

1.	Wstęp

W dzisiejszych czasach ludzie coraz częściej robią zakupy online, dlatego wychodzimy naprzeciw oczekiwaniom naszych klientów i udostępniamy do ich dyspozycji stronę internetowa, która umożliwi zakupy z dowolnego miejsca na świecie. 
Projekt obejmuje stworzenie strony internetowej z artykułami elektronicznymi, w której klient ma możliwość wyboru oraz zakupu produktów elektronicznych. Użytkownik ma możliwość rejestrowania się na stronie oraz logowania. Posiada możliwość tworzenia koszyka w którym może dodawać lub usuwać przedmioty. Użytkownik zalogowany dodatkowo ma dostęp do historii swoich zakupów. Sklep pozwala na różne formy płatności. 


2. Case Study
Aktorzy:

• Użytkownik niezalogowany 

• Użytkownik zalogowany

• Administrator

Role aktorów

Użytkownik niezalogowany

•	Ma możliwość przeglądania produktów:

 o 	Nazwa 

 o	Opis 

 o	Cena



•	Może się zarejestrować podając:

 o	Login*

 o	Hasło*

 o	Adres e-mail*

 o	Adres zamieszkania

 o	Numer telefonu


•	Może się zalogować podając:

 o	Login/E-mail*

 o	Hasło*

 

Użytkownik zalogowany

•	Ma możliwość przeglądania produktów:

 o	Nazwa 

 o	Opis 

 o	Cena



•	Może złożyć zamówienie podając:

 o	Adres zamieszkania*

 o	Numer telefonu*

•	Może przeglądać historie zamówień

•	Może zmienić dane takie jak:

 o	Adres

 o	Login

 o	Hasło

 o	Numer Telefonu

•	Może się wylogować

  
Administrator

•	Może wyświetlać listę użytkowników.

•	Może zarządzać produktami:

 o	Dodać produkty

 o	Usunąć produkty

 o	Edytować produkty


•	Może zmienić dane takie jak:

 o	Adres

 o	Login

 o	Hasło

 o	Numer Telefonu

•	Może się wylogować
